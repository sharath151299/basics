package basics.day52.oops.abstraction.withabstractclass;

public abstract class Calculator1 {
	void add(int a, int b) {
		System.out.println("Default implementation from abstract class Calculator - sum is: " + (a + b));
	}

	abstract void subtract(int a, int b);

	/**
	 * 1. Add multiply() method similar to add() implemented above
	 * 
	 * 2. Add divide() method similar to subtract() defined above
	 * 
	 */
}
