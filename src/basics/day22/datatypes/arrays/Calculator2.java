package basics.day22.datatypes.arrays;

import java.util.Scanner;

public class Calculator2 {

	/**
	 * 1. Implement a calculator. Accept 3 inputs from the console and store them
	 * into array (instead of storing into individual variables). Sample provided
	 * below
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		float[] inputNumbers = new float[3];

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first number:");
		System.out.println();
		inputNumbers[0] = sc.nextFloat();

		sc.close();
	}

}
