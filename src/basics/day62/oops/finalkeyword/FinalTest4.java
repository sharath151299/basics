package basics.day62.oops.finalkeyword;

public class FinalTest4 {

	public static void main(String[] args) {
		/**
		 * 1. Observe the behavior after making the method add() as final in
		 * Calculator3.java
		 */
		Calculator3 calc = new Calculator3Impl4();
		calc.add(10, 15);

	}

}
