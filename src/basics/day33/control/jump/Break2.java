package basics.day33.control.jump;

public class Break2 {

	/**
	 * 1. Create a for loop which iterates (variable i) from 1 to 10. Create an
	 * inner for loop which also iterates (variable j) from 1 to 10. Exit the inner
	 * loop if the value of i becomes equal to j
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 10; j++) {
				System.out.println(i + " " + j);
			}
		}

	}

}
